<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/20/2019
 * Time: 1:51 PM
 */

namespace Bitm\Car;


class Car
{
    public $comp;
    public $color = "beige";
    public $hasSunRoof = true;

    public function hello(){
        return " Hello,Jane ";
    }

    public function register(){
        echo  $this->hello() ." >> registered";
    }

    public function mail(){
        return $this->register() . ">> mail sent ";
    }


}