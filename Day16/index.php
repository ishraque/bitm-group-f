<?php

function br(){
    echo "<br><br>";
}

//echo "Hello world";

include_once("vendor/autoload.php");

use Bitm\Greeting\Greeting;
use Bitm\Customer\Person;
use Bitm\Vehicle\Vehicle;
use Bitm\Vehicle\LandVehicle;
use Bitm\Vehicle\WaterVehicle;
use Bitm\Vehicle\AirVehicle;


$truck = new LandVehicle();
$truck->move();
br();
$lonch = new WaterVehicle();
$lonch->move();
br();
$airbus = new AirVehicle();
$airbus->move();
br();
echo $airbus->hasRoof();
br();
//$greeting1 = new greeting;
//$greeting1->sayHello();

//
//$person1 = new Person("Mr. ","Ishraqul ", "Hoque");
//
//
////echo $person1->title." ".$person1->firstname." ".$person1->lastname;
//echo $person1;
br();


