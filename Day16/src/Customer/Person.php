<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/18/2019
 * Time: 4:05 PM
 */

namespace Bitm\Customer;

class Person{

    public $title = "Title";
    public $firstname = "Default First name";
    public $lastname= "Default First name";
    private $salary = 100000;

    public function __construct($title, $firstname, $lastname){
        $this->title = $title;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }


    public function work(){
        return "I am working";
    }

    public function getSalary(){
        return $this->salary;
    }

    public function fullname(){
        return $this->title." ".$this->firstname." ".$this->lastname;
    }

    public function __toString()
    {
     return $this->fullname();
    }
}
