<?php
// 5. What will be the output of the following code?
//$a =1;
//
//if($a--)
//    print "true";
//if($a++)
//    print "False";

// 7. What will be the output of the following code?
//$a =1;
//
//if(print $a)
//    print "true";
//else
//    print "False";

// 8. What will be the output of the following code?
//$a =10;
//
//if(1)
//    print "all";
//else
//    print "some";
//else
//    print "none";

// 9. What will be the output of the following code?
//$a =10;
//
//if(0)
//    print "all";
//if
//else
//    print "some";


// 10. What will be the output of the following code?
//$a ="";
//
//if($a)
//    print "all";
//if
//else
//    print "some";


// 11. What will be the output of the following code?
//$a ="a";
//
//if($a)
//    print "all";
//if
//else
//    print "some";

// 12. What will be the output of the following code?
//$foo ="Bob";
//$bar = &$foo;
//$bar = "My name is $bar";


// 13. What will be the output of the following code?
//$color = "maroon";
//$var = $color[2];
//echo "$var";

// 14. What will be the output of the following code?

//$score = 1234;
//$scoreboard = (array)$score;
//echo $scoreboard[0];

// 15. What will be the output of the following code?

//function track(){
//    static $count =0;
//    $count++;
//    echo $count;
//}
//track();
//track();
//track();

// 16. What will be the output of the following code?

//$a = "clue";
//$a = "get";
//echo "$a";

// 17. What will be the output of the following code?

//$states = array("Karnataka" => array(
//    "population" => "11,35,000", "capital" => "Banglalore",
//    "Tamil Nadu" => array( "population" => "17,90,000"),
//    "capital" => "Chennai"
//));
//
//echo $states["Karnataka"]["population"];

// 18. What will be the output of the following code?

//$state = array("Karnataka", "Goa", "Tamil Nadu", "Andhra Pradesh");
//echo (array_search("Tamil Nadu", $state));

// 19. What will be the output of the following code?

//$fruits = array("apple", "Orange", "Banana");
//echo (next($fruits));
//echo (next($fruits));

// 20. What will be the output of the following code?

//$fruits = array("apple", "orange", array("pear", "mango"),
//    "Banana");
//echo (count($fruits, 1));

// 21. What will be the output of the following code?

//$cars = array("Volvo", "BMW", "Toyota");
//echo "I like ".$cars[2].", ".$cars[1]." and "." . ";

// 22. What will be the output of the following code?

//$a = array("A", "Cat", "Dog", "A", "Dog");
//$b = array("A", "A", "Cat", "A", "Tiger");
//$c = array_combine($a, $b);
//print_r(array_count_values($c));

// 23. What will be the output of the following code?

//$a1 = array("a" => "red", "b" => "green", "c" => "blue", "d" => "yellow");
//$a2 = array("e" => "red", "f" => "green", "g" => "blue", "h" => "yellow");
//$a3 = array("i" => "orange");
//$a4 = array_merge($a, $a3);
//$result = array_diff($a1, $a4);
//print_r($result);
//

// 24. What will be the output of the following code?

//$a = 10;
//echo ++$a;
//echo $a++;
//echo $a;
//echo ++$a;


// 25. What will be the output of the following code?

//$a = 12;
//--$a;
//echo $a++;
//
