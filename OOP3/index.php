<?php
function br(){
    echo "<br><br>";
}
include_once("vendor/autoload.php");

use Bitm\Triangle;
use Bitm\Rectangle;
use Bitm\Circle;
use Bitm\Car;
use Bitm\Fruit;
use Bitm\Course;
use Bitm\Toyota;
use Bitm\Honda;
use Bitm\Admin;


$triangle = new Triangle();
$triangle->setBase(23);
$triangle->setHeight(26);
echo "(a) The  Area of Triangle is ".$triangle->getArea()." sqcm";
br();

$Rectangle = new Rectangle();
$Rectangle->setLength(2);
$Rectangle->setWidth(1);
echo "1. The area of Rectangle is ".$Rectangle->getArea()." sqft";
br();
$circle = new Circle();
$circle->setRadius(2);
echo "2. The area of Circle is ".$circle->getArea()." sqcm";
br();

$Rectangle->setHeight(4);
echo "3. The Volume of Rectangle is ".$Rectangle->getVolume()." cubicft";

br();
br();

$bmw = new Car();
echo "BMW :".$bmw->forword();
br();
$mango = new Fruit();
echo "Mango: ".$mango->isSweet("mango");
br();

$bitm = new Course();
echo $bitm->hasOffered("php");
br();

$car1 = new Toyota(10);
$car1->setTankVolume(10);
echo "The number of miles: ".$car1->calcNumMilesOnFullTank()."& The car's color: ".$car1->getColor();
br();

$hello = new Admin();
$hello->setUsername("Balthazer");
echo $hello->getUsername()." is ".$hello->stateYourRole();
br();


