<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/23/2019
 * Time: 1:59 PM
 */

namespace Bitm;


class Triangle
{
public $base = 0;
public $height = 0;
public function getArea(){
    return ($this->base*$this->height)/2;
}

public function setBase($base){
    $this->base = $base;
}
public function setHeight($height){
    $this->height = $height;
}
}