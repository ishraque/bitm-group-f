<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/23/2019
 * Time: 2:27 PM
 */

namespace Bitm;


class Circle
{
   public $pi = 3.141516;
   public $radius = 0;

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;
    }

    public function getArea(){
        return ($this->pi*$this->radius*$this->radius)/2;
    }
}