<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/23/2019
 * Time: 5:00 PM
 */

namespace Bitm;


abstract class User
{
    protected $username;

   abstract public function stateYourRole();

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
}