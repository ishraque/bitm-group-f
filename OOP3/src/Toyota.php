<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/23/2019
 * Time: 4:34 PM
 */

namespace Bitm;

class Toyota extends Car
{

    private $color = "Blue";


    public function calcNumMilesOnFullTank()
    {
        $miles = $this->tankVolume*30;
        return $miles;
    }

    public function getColor(){
        return $this->color;
    }
}