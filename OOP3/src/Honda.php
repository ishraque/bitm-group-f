<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 6/23/2019
 * Time: 4:32 PM
 */

namespace Bitm;


class Honda extends Car
{
 public function calcNumMilesOnFullTank()
 {
     $miles = $this->tankVolume*30;
     return $miles;
 }
}